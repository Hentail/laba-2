import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char a;
        int b = 1, c = 0;
        int title;
        List<Bookes> books = new ArrayList<>();
        books.add(new Bookes("Книга1","Автор1",1890));
        books.add(new Bookes("Книга2","Автор2",1891));
        books.add(new Bookes("Книга3","Автор3",1892));
        books.add(new Bookes("Книга4","Автор4",1893));
        books.add(new Bookes("Книга5","Автор5",1894));
        books.add(new Bookes("Книга6","Автор6",1895));
        books.add(new Bookes("Книга7","Автор7",1896));
        books.add(new Bookes("Книга8","Автор8",1897));
        while (true) {
            System.out.println("Выберите действие:\n1.Поиск книги по названию\n2.Добавить книгу\n3.Удалить книгу\n4.Вывод всех книг");
            a = scanner.next().charAt(0);
            switch (a) {
                case '1': {
                    while (b != 0) {
                        System.out.print("Введите год публикации книги:");
                        title = scanner.nextInt();
                        for (int i = 0; i < books.size(); i++) {
                            if (books.get(i).getYear() == title) {
                                System.out.printf(books.get(i).toString());
                                c++;
                            }
                        }
                        if (c == 0) {
                            System.out.printf("В системе нет книги с таким годом публикации\n");
                        }
                        System.out.println("Продолжить? (д/н)");
                        b = scanner.next().charAt(0);
                        switch (b) {
                            case 'д':
                                b = 0;
                                break;
                            case 'н': {
                                b = 1;
                                break;
                            }
                        }
                    }
                    break;
                }
                case '2':{
                    String name, autor;
                    int year;
                    System.out.printf("Введите автора: ");
                    autor = scanner.next();
                    System.out.printf("Введите название книги: ");
                    name = scanner.next();
                    System.out.printf("Введите год издания: ");
                    year = scanner.nextInt();
                    books.add(new Bookes(name, autor, year));
                    break;
                }
                case '3':{
                    for (int i = 0; i < books.size(); i++) {
                        System.out.printf((i + 1)+ ". " + books.get(i).toString());
                    }
                    System.out.printf("Введите id книги, которую хотите удалить\n");
                    int j = scanner.nextInt();
                    books.remove(j - 1);
                    break;
                }
                case '4':{
                    for (int i = 0; i < books.size(); i++) {
                        System.out.printf((i + 1)+ ". " + books.get(i).toString());
                    }
                    break;
                }

            }
        }

    }
}
